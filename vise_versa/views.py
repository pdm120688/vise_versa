from django.http import HttpResponse
from django.shortcuts import render

def home(request):
    return render(request, 'home.html',)

def reverse(request):
    user_text = request.GET['usertext']
    reverse_text = user_text[::-1]
    words = user_text.split()
    words_count = len(words)
    return render(request, 'reverse.html',{'usertext': user_text, 'reversedtext': reverse_text,'words_count':words_count,})
